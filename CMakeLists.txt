cmake_minimum_required(VERSION 3.15)
project(atmSA C)

set(CMAKE_C_STANDARD 99)

add_executable(atmSA main.c)